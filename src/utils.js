export const getBookieIndex = (bookies, id) => {
	const selectedBookie = bookies.find((bookie) => bookie.id === id);
	const idx = bookies.indexOf(selectedBookie);

	return idx;
}
