import Vue from 'vue';
import Vuex from 'vuex';

import { getBookieIndex } from './utils';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		bookies: [],
		countries: [],
		filterTerm: 'all',
	},
	
	mutations: {
		SET_COUNTRIES(state, payload) {
			state.countries = payload;
		},

		SET_BOOKIES(state, payload) {
			state.bookies = payload;

			const computedBookies = payload.map(item => {
				if(item.links === undefined) {
					item.links = {
						default: ''
					}
				}

				return item;
			});

			state.bookies = computedBookies;
		},

		SET_BOOKIE_ACTIVE_STATE(state, payload) {
			const idx = getBookieIndex(state.bookies, payload.id);

			state.bookies[idx].active = payload.active;
		},

		SET_BOOKIES_ACTIVE_STATE(state, payload) {
			state.bookies.forEach(item => {
				item.active = payload;
			})
		},

		SET_BOOKIE_LINK_DEFAULT(state, payload) {
			const idx = getBookieIndex(state.bookies, payload.id);

			state.bookies[idx].links.default = payload.linkDefault;
		},

		SET_FILTER_TERM(state, payload) {
			state.filterTerm = payload;
		},

		SET_BOOKIE_LINKS(state, payload) {
			const idx = getBookieIndex(state.bookies, payload.id);

			const linkDefault = state.bookies[idx].links.default;

			state.bookies[idx].links = {
				'default': linkDefault,
				...payload.links
			}
		},

		SET_BOOKIES_ORDER(state, payload) {
			state.bookies = payload;
		},
	},
	
	actions: {
		fetchCountries({ commit }) {
			fetch('./data/countries.json')
				.then((response) => {
					return response.json()
				})
				.then((data) => {
					commit('SET_COUNTRIES', data);
				});
		},

		fetchBookies({ commit }) {
			fetch('./data/bookies.json')
				.then((response) => {
					return response.json()
				})
				.then((data) => {
					commit('SET_BOOKIES', data);
				});
		},

		updateBookieActiveState({ commit }, payload) {
			commit('SET_BOOKIE_ACTIVE_STATE', payload)
		},

		updateBookieLinkDefault({ commit }, payload) {
			commit('SET_BOOKIE_LINK_DEFAULT', payload)
		},

		updateFilterTerm({ commit }, payload) {
			commit('SET_FILTER_TERM', payload);
		},

		updateBookieLinks({ commit }, payload) {
			commit('SET_BOOKIE_LINKS', payload);
		},

		toggleBookiesActiveState({ commit }, payload) {
			commit('SET_BOOKIES_ACTIVE_STATE', payload);
		},

		updateBookiesOrder({ commit }, payload) {
			commit('SET_BOOKIES_ORDER', payload);
		},
	},

	getters: {
		bookies: state => state.bookies,
		
		countries: state => state.countries,
		
		filterTerm: state => state.filterTerm,

		filteredBookies: (state, getters) => {
			const { filterTerm } = state;

			const allBookies = state.bookies;
			const activeBookies = allBookies.filter(item => item.active === 1);
			const inactiveBookies = allBookies.filter(item => item.active === 0);

			const specificBookie = allBookies.filter(item => item.slug === filterTerm);

			switch(filterTerm) {
				case 'all': 
					return allBookies;
					break;

				case 'active':
					return activeBookies;
					break;

				case 'inactive':
					return inactiveBookies
					break;

				default:
					return specificBookie;
			}
		}
	}
});
